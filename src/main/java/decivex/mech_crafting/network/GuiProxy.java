package decivex.mech_crafting.network;

import decivex.mech_crafting.mechanical_workbench.TileEntityMechanicalWorkbench;
import decivex.mech_crafting.mechanical_workbench.GuiMechanicalWorkbench;
import decivex.mech_crafting.mechanical_workbench.ContainerMechanicalWorkBench;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiProxy implements IGuiHandler {

    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity te = world.getTileEntity(pos);
        if (te instanceof TileEntityMechanicalWorkbench) {
            return new ContainerMechanicalWorkBench(player.inventory, (TileEntityMechanicalWorkbench) te);
        }
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity te = world.getTileEntity(pos);
        if (te instanceof TileEntityMechanicalWorkbench) {
            TileEntityMechanicalWorkbench containerTileEntity = (TileEntityMechanicalWorkbench) te;
            return new GuiMechanicalWorkbench(new ContainerMechanicalWorkBench(player.inventory, containerTileEntity));
        }
        return null;
    }
}