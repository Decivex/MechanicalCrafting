package decivex.mech_crafting.integration.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.IModRegistry;

@mezz.jei.api.JEIPlugin
public class MechCraftingJEIPlugin implements IModPlugin {
    public MechCraftingJEIPlugin() {
        super();
    }

    @Override
    public void register(IModRegistry registry) {
        registry.getRecipeTransferRegistry().addRecipeTransferHandler(new MWBTransferInfo());
    }
}
