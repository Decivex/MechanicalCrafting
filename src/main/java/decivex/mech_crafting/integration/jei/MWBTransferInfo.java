package decivex.mech_crafting.integration.jei;

import decivex.mech_crafting.mechanical_workbench.ContainerMechanicalWorkBench;
import mezz.jei.api.recipe.VanillaRecipeCategoryUid;
import mezz.jei.api.recipe.transfer.IRecipeTransferInfo;
import net.minecraft.inventory.Slot;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MWBTransferInfo implements IRecipeTransferInfo<ContainerMechanicalWorkBench> {
    @NotNull
    @Override
    public Class<ContainerMechanicalWorkBench> getContainerClass() {
        return ContainerMechanicalWorkBench.class;
    }

    @NotNull
    @Override
    public String getRecipeCategoryUid() {
        return VanillaRecipeCategoryUid.CRAFTING;
    }

    @Override
    public boolean canHandle(@NotNull ContainerMechanicalWorkBench container) {
        return true;
    }

    @NotNull
    @Override
    public List<Slot> getRecipeSlots(ContainerMechanicalWorkBench container) {
        return container.craftingSlots;
    }

    @NotNull
    @Override
    public List<Slot> getInventorySlots(ContainerMechanicalWorkBench container) {
        return container.playerSlots;
    }
}
