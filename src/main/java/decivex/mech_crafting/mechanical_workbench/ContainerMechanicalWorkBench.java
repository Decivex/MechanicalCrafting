package decivex.mech_crafting.mechanical_workbench;

import decivex.mech_crafting.MechCraftingMod;
import decivex.mech_crafting.network.ModPacketHandler;
import decivex.mech_crafting.network.PacketUpdateItemStack;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IContainerListener;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;
import org.apache.logging.log4j.Level;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;

public class ContainerMechanicalWorkBench extends Container implements PacketUpdateItemStack.IUpdateNonSlotItemStack {
    private final TileEntityMechanicalWorkbench mct;
    public static final int MCT_SIZE = 9;

    public int progress;
    public int requiredProgress;
    public ItemStack result;

    public List<Slot> craftingSlots = new ArrayList<>();
    public List<Slot> playerSlots = new ArrayList<>();

    public ContainerMechanicalWorkBench(IInventory playerInventory, TileEntityMechanicalWorkbench mct) {
        this.mct = mct;
        this.result = mct.getResult();

        addMCTSlots();
        addPlayerSlots(playerInventory);
    }

    @Override
    public void addListener(IContainerListener listener) {
        super.addListener(listener);
        listener.sendWindowProperty(this, 0, mct.getAdjustedProgress());
        listener.sendWindowProperty(this, 1, mct.requiredProgress);
        sendResultStack(listener, mct.getResult());
    }

    public void sendResultStack(IContainerListener listener, ItemStack stack) {
        if(!(listener instanceof EntityPlayerMP)) return;

        ModPacketHandler.INSTANCE.sendTo(
                new PacketUpdateItemStack(this, 0, mct.getResult()),
                (EntityPlayerMP) listener
        );
    }

    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        boolean stackChanged = !ItemStack.areItemStackTagsEqual(this.result, this.mct.getResult());

        for (IContainerListener listener : listeners) {
            if (stackChanged) {
                sendResultStack(listener, mct.getResult());
            }

            if (this.progress != mct.getAdjustedProgress()) {
                listener.sendWindowProperty(this, 0, mct.getAdjustedProgress());
            }

            if (this.requiredProgress != mct.requiredProgress) {
                listener.sendWindowProperty(this, 1, mct.requiredProgress);
            }
        }

        this.progress = mct.getAdjustedProgress();
        this.requiredProgress = mct.requiredProgress;
        this.result = mct.getResult();
    }

    /**
     * This method has a misleading name, it actually processes the property
     * update packets that servers send to clients.
     * @param id the id of the property to update
     * @param data the new property value
     */
    @Override
    public void updateProgressBar(int id, int data) {
        switch (id) {
            case 0:
                this.progress = data;
                break;
            case 1:
                this.requiredProgress = data;
                break;
            default:
                MechCraftingMod.instance.logger.log(Level.ERROR, "Unknown property: "+id);
                break;
        }
    }

    private void addPlayerSlots(IInventory playerInventory) {
        // Slots for the main inventory
        for (int row = 0; row < 3; ++row) {
            for (int col = 0; col < 9; ++col) {
                int x = 8 + col * 18;
                int y = row * 18 + 84;
                playerSlots.add(this.addSlotToContainer(new Slot(playerInventory, col + row * 9 + 9, x, y)));
            }
        }

        // Slots for the hotbar
        for (int idx = 0; idx < 9; ++idx) {
            int x = 8 + idx * 18;
            int y = 142;
            playerSlots.add(this.addSlotToContainer(new Slot(playerInventory, idx, x, y)));
        }
    }

    private void addMCTSlots() {
        IItemHandler itemHandler = this.mct.getCapability(ITEM_HANDLER_CAPABILITY, null);

        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                Slot s = this.addSlotToContainer(new SlotItemHandler(
                    itemHandler,
                    j + i * 3,
                    30 + j * 18,
                    17 + i * 18
                ));
                craftingSlots.add(s);
            }
        }
    }

    @NotNull
    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < MCT_SIZE) {
                // Item is in MCT, move to inventory
                if (!this.mergeItemStack(itemstack1, MCT_SIZE, this.inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemstack1, 0, MCT_SIZE, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.isEmpty()) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }
        }

        return itemstack;
    }

    @Override
    public boolean canInteractWith(@NotNull EntityPlayer playerIn) {
        return true;
    }

    @Override
    public void updateItem(int idx, ItemStack stack) {
        this.result = stack;
    }
}
