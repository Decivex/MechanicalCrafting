package decivex.mech_crafting.mechanical_workbench;

import decivex.mech_crafting.MechCraftingMod;
import decivex.mech_crafting.ModConfig;
import decivex.mech_crafting.client.BaseGui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.ResourceLocation;

public class GuiMechanicalWorkbench extends BaseGui {
    private static final ResourceLocation background = new ResourceLocation(
            MechCraftingMod.MODID, "textures/gui/container/mechanical_crafter.png"
    );

    private final ContainerMechanicalWorkBench container;

    public GuiMechanicalWorkbench(ContainerMechanicalWorkBench inventorySlotsIn) {
        super(inventorySlotsIn);
        this.container = inventorySlotsIn;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        mc.getTextureManager().bindTexture(background);

        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

        int progress = this.getProgressScaled(24);
        this.drawTexturedModalRect(
                guiLeft + 89, guiTop + 34,
                176, 0,
                progress + 1, 16
        );
    }

    private int getProgressScaled(int pixels) {
        if(container.progress == 0 || container.requiredProgress == 0) return 0;
        return container.progress * pixels / container.requiredProgress;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);

        RenderHelper.enableGUIStandardItemLighting();
        GlStateManager.pushMatrix();
        GlStateManager.translate((float)this.guiLeft, (float)this.guiTop, 0.0F);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        GlStateManager.enableRescaleNormal();

        drawItem(container.result, 124, 35);

        GlStateManager.popMatrix();

        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void renderHoveredToolTip(int mouseX, int mouseY) {
        if(!this.mc.player.inventory.getItemStack().isEmpty()) return;

        if(getSlotUnderMouse() != null) {
            super.renderHoveredToolTip(mouseX, mouseY);
        }
        else if(!container.result.isEmpty()
                && isPointInRegion(124, 35, 16, 16, mouseX, mouseY)) {
            this.renderToolTip(container.result, mouseX, mouseY);
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.fontRenderer.drawString(I18n.format("container.mechanical_crafter"), 28, 6, 4210752);
        this.fontRenderer.drawString(I18n.format("container.inventory"), 8, this.ySize - 96 + 2, 4210752);

        if(!ModConfig.DEBUG) return;

        String progressString = String.format(
                "Progress: %d/%d\nResult: %s\nResult empty: %s",
                container.progress,
                container.requiredProgress,
                container.result.getDisplayName(),
                container.result.isEmpty()
        );
        this.fontRenderer.drawSplitString(
                progressString, 8, 8, 80, 255 << 16
        );
    }
}
