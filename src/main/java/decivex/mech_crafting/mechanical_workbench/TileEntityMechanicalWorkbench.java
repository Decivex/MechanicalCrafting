package decivex.mech_crafting.mechanical_workbench;

import decivex.mech_crafting.ModConfig;
import decivex.mech_crafting.RegistryHandler;
import decivex.mech_crafting.inventory.ExistingOnlyItemHandlerWrapper;
import decivex.mech_crafting.inventory.InventoryCraftingWrapper;
import decivex.mech_crafting.inventory.QueueItemHandler;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;

import static mysticalmechanics.api.MysticalMechanicsAPI.MECH_CAPABILITY;
import static net.minecraftforge.items.CapabilityItemHandler.ITEM_HANDLER_CAPABILITY;

public class TileEntityMechanicalWorkbench extends TileEntity implements ITickable {
    public InventoryCraftingWrapper inventory;
    public InputMechCapability mechCapability;
    ItemStack result;
    public int progress;
    public int requiredProgress;
    public int outputCooldown;
    public QueueItemHandler outputQueue;

    public TileEntityMechanicalWorkbench() {
        super();
        inventory = new MechanicalWorkbenchItemCapabillity();
        mechCapability = new InputMechCapability();
        result = ItemStack.EMPTY;
        progress = 0;
        requiredProgress = 0;
        outputCooldown = 0;
        outputQueue = new QueueItemHandler();
    }

    @Override
    public boolean hasCapability(@NotNull Capability<?> capability, @Nullable EnumFacing facing) {
        if (capability == ITEM_HANDLER_CAPABILITY) {
            return true;
        }

        if (capability == MECH_CAPABILITY && facing != null && facing.getAxis() == findDirection().rotateY().getAxis()) {
            return true;
        }

        return super.hasCapability(capability, facing);
    }

    @Override
    public <T> T getCapability(@NotNull Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == ITEM_HANDLER_CAPABILITY) {
            return (T) (facing == null ? inventory : new ExistingOnlyItemHandlerWrapper(inventory));
        }

        if (capability == MECH_CAPABILITY && facing != null && facing.getAxis() == findDirection().rotateY().getAxis()) {
            return (T) mechCapability;
        }
        return super.getCapability(capability, facing);
    }

    public int getNumberOfIngredients() {
        int count = 0;
        for(int i=0; i<inventory.getSlots(); i++) {
            if(!inventory.getStackInSlot(i).isEmpty()) {
                count++;
            }
        }
        return count;
    }

    public int getProgressRequired() {
        return ModConfig.mechanicalCrafter.basePowerCost + ModConfig.mechanicalCrafter.ingredientPowerCost * getNumberOfIngredients();
    }

    public double getScaledProgress() {
        return progress / (double) requiredProgress;
    }

    public int getAdjustedProgress() {
        if(outputQueue.isEmpty()) {
            return progress;
        }
        else {
            return requiredProgress;
        }
    }

    public ItemStack getResult() {
        return result;
    }

    @Override
    public void update() {
        if(world.isRemote) return;

        if(outputCooldown > 0) {
            outputCooldown--;
        }

        if(!outputQueue.isEmpty()) {
            if(outputCooldown <= 0) {
                outputItemFromQueue();
                outputCooldown = ModConfig.mechanicalCrafter.outputCooldown;
            }
            return;
        }

        if(canProgress()) {
            progress += mechCapability.power;

            if(progress >= requiredProgress) {
                tryCrafting();
            }
        } else {
            progress = 0;
        }
    }

    public boolean canProgress() {
        if (getResult().isEmpty()) return false;

        for (int i=0; i<inventory.getSlots(); i++) {
            ItemStack stack = inventory.getStackInSlot(i);
            if (stack.getCount() == 1 && !(stack.getMaxStackSize() == 1)) return false;
        }

        return true;
    }

    public EnumFacing findDirection() {
        IBlockState bs = world.getBlockState(pos);
        return bs.getBlock() == RegistryHandler.MECHANICAL_CRAFTER ?
                bs.getValue(BlockMechanicalWorkbench.FACING) : EnumFacing.DOWN;
    }

    public void tryCrafting() {
        progress = 0;
        IRecipe recipe = CraftingManager.findMatchingRecipe(inventory.getCraftingGrid(), world);

        if (recipe == null) return;

        // TODO: Use fake player to call onCrafting
        // TODO: Modify underlying data structure so the onchanged method doesn't get called up to 9 times
        outputQueue.push(recipe.getCraftingResult(inventory.getCraftingGrid()));
        NonNullList<ItemStack> remaining = recipe.getRemainingItems(inventory.getCraftingGrid());


        for(int slot = 0; slot < inventory.getSlots(); slot++) {
            ItemStack remainingItem = remaining.get(slot);
            ItemStack ingredient = inventory.getStackInSlot(slot);

            if(remainingItem.isEmpty()) {
                inventory.extractItem(slot, 1, false);
            }
            else if(ItemStack.areItemsEqual(remainingItem, ingredient) &&
                    ItemStack.areItemStackTagsEqual(remainingItem, ingredient)) {
                continue;
            }
            else if(ingredient.isItemStackDamageable()
                    && ingredient.isItemEqualIgnoreDurability(remainingItem)
                    && ingredient.getItemDamage() < remainingItem.getItemDamage()) {
                inventory.setStackInSlot(slot, remainingItem);
            }
            else {
                outputQueue.push(remainingItem);
                inventory.extractItem(slot, 1, false);
            }
        }
    }

    private void outputItemFromQueue() {
        if (outputQueue.isEmpty()) return;

        ItemStack stack = outputQueue.peek();
        stack = outputItem(stack);

        if (stack.isEmpty()) {
            outputQueue.pop();
        }
        else {
            outputQueue.replaceHead(stack);
        }
    }

    private ItemStack outputItem(ItemStack stack) {
        EnumFacing outputDirection = findDirection();

        TileEntity te = world.getTileEntity(pos.offset(outputDirection));
        IItemHandler destination = null;

        if(te != null && te.hasCapability(ITEM_HANDLER_CAPABILITY, outputDirection.getOpposite())) {
            destination = te.getCapability(ITEM_HANDLER_CAPABILITY, outputDirection.getOpposite());
        }

        if (destination != null) {
            stack = ItemHandlerHelper.insertItem(destination, stack, false);
            return stack;
        }

        dropItem(stack, outputDirection);
        return ItemStack.EMPTY;
    }

    private void dropItem(ItemStack stack, EnumFacing direction) {
        double x = pos.getX() + 0.5 + direction.getFrontOffsetX() * 0.625;
        double y = pos.getY() + ((direction.getAxis() == EnumFacing.Axis.Y) ? 0.5 : 0.125)
                + direction.getFrontOffsetY() * 0.625;
        double z = pos.getZ() + 0.5 + direction.getFrontOffsetZ() * 0.625;
        double speed = ModConfig.mechanicalCrafter.ejectionVelocity;
        double sx = direction.getFrontOffsetX() * speed;
        double sy = direction.getFrontOffsetY() * speed;
        double sz = direction.getFrontOffsetZ() * speed;

        EntityItem entity = new EntityItem(world, x, y, z, stack);
        entity.motionX = sx;
        entity.motionY = sy;
        entity.motionZ = sz;
        world.spawnEntity(entity);
    }

    private class MechanicalWorkbenchItemCapabillity extends InventoryCraftingWrapper {
        @Override
        protected void onContentsChanged(int slot) {
            super.onContentsChanged(slot);

            if(world == null || world.isRemote) return;

            IRecipe recipe = CraftingManager.findMatchingRecipe(this.getCraftingGrid(), world);

            ItemStack newResult = recipe == null ? ItemStack.EMPTY : recipe.getCraftingResult(this.getCraftingGrid());
            int newRequiredProgress = getProgressRequired();

            if(!ItemStack.areItemStackTagsEqual(newResult, getResult())
                    || newRequiredProgress != requiredProgress) {
                result = newResult;
                progress = 0;
                requiredProgress = getProgressRequired();
            }

            TileEntityMechanicalWorkbench.this.markDirty();
        }
    }

    @NotNull
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        compound.setTag("inventory", inventory.serializeNBT());
        compound.setInteger("progress", progress);
        compound.setInteger("required_progress", requiredProgress);
        compound.setTag("result", result.serializeNBT());
        compound.setInteger("output_cooldown", outputCooldown);
        compound.setTag("output_queue", outputQueue.serializeNBT());
        compound.setTag("mech", mechCapability.serializeNBT());
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        inventory.deserializeNBT(compound.getCompoundTag("inventory"));
        progress = compound.getInteger("progress");
        requiredProgress = compound.getInteger("required_progress");
        result = new ItemStack(compound.getCompoundTag("result"));
        outputQueue.deserializeNBT(compound.getTagList("output_queue", 10));
        mechCapability.deserializeNBT(compound.getCompoundTag("mech"));
    }
}
