package decivex.mech_crafting.mechanical_bellows;

import decivex.mech_crafting.ModConfig;
import decivex.mech_crafting.mechanical_workbench.InputMechCapability;
import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityFurnace;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.common.capabilities.Capability;
import org.jetbrains.annotations.Nullable;

import static mysticalmechanics.api.MysticalMechanicsAPI.MECH_CAPABILITY;

public class TileEntityMechanicalBellows extends TileEntity implements ITickable {
    public InputMechCapability mechCapability;
    public double currentRotation;
    public double totalRotation;

    public TileEntityMechanicalBellows() {
        super();
        mechCapability = new InputMechCapability() {
            @Override
            public void onPowerChange() {
                super.onPowerChange();
                markDirty();
                if (world != null) {
                    IBlockState state = world.getBlockState(getPos());
                    world.notifyBlockUpdate(getPos(), state, state, 3);
                }
            }
        };
        currentRotation = 0.0;
        totalRotation = 0.0;
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if (capability == MECH_CAPABILITY && facing != null) {
            EnumFacing.Axis axis = facing.getAxis();
            return axis.isHorizontal() && axis != findDirection().getAxis();
        }

        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == MECH_CAPABILITY && facing != null) {
            EnumFacing.Axis axis = facing.getAxis();
            if (axis.isHorizontal() && axis != findDirection().getAxis()) {
                return (T) mechCapability;
            }
        }

        return super.getCapability(capability, facing);
    }

    public void heatFurnace(TileEntityFurnace furnace, int progress) {
        int cookTime = furnace.getField(2);
        if (cookTime==0) return;
        int totalCookTime = furnace.getField(3);
        cookTime = Math.min(cookTime+progress, totalCookTime-1);
        furnace.setField(2, cookTime);
    }

    public EnumFacing findDirection() {
        return EnumFacing.getHorizontal(this.getBlockMetadata());
    }

    public double getRotationSpeed() {
        return Math.min(
                ModConfig.mechanicalBellows.rotationCap,
                mechCapability.power * ModConfig.mechanicalBellows.powerFactor
        );
    }

    @Override
    public void update() {
        double addRotation = getRotationSpeed();
        currentRotation = (currentRotation + addRotation) % 360.0;

        if (!world.isRemote) {
            totalRotation += addRotation;

            EnumFacing direction = findDirection();
            BlockPos target = this.pos.offset(direction);
            TileEntity te = world.getTileEntity(target);

            if (te instanceof TileEntityFurnace) {
                double rpbt = ModConfig.mechanicalBellows.rotationPerBurnTick;
                if (totalRotation >= rpbt) {
                    double newTotal = totalRotation % rpbt;
                    int ticksToAdd = (int) ((totalRotation-newTotal) / rpbt);
                    heatFurnace((TileEntityFurnace) te, ticksToAdd);
                    totalRotation = newTotal;
                }
            }
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        mechCapability.writeToNBT(compound);
        compound.setDouble("rotation", currentRotation);
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        mechCapability.readFromNBT(compound);
        currentRotation = compound.getDouble("rotation");
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        return writeToNBT(new NBTTagCompound());
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        NBTTagCompound nbtTag = new NBTTagCompound();
        this.writeToNBT(nbtTag);
        return new SPacketUpdateTileEntity(getPos(), 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity packet) {
        this.readFromNBT(packet.getNbtCompound());
    }
}
