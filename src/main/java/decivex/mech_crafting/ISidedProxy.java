package decivex.mech_crafting;

import decivex.mech_crafting.network.PacketUpdateItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public interface ISidedProxy {
    
    public void handleUpdateItemStack(PacketUpdateItemStack message, MessageContext context);
    
}