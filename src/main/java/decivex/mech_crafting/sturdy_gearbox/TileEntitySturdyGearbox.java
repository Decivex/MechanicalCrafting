package decivex.mech_crafting.sturdy_gearbox;

import blusunrize.immersiveengineering.api.energy.IRotationAcceptor;
import decivex.mech_crafting.ModConfig;
import mysticalmechanics.api.DefaultMechCapability;
import mysticalmechanics.api.IMechCapability;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.fml.common.Optional;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static mysticalmechanics.api.MysticalMechanicsAPI.MECH_CAPABILITY;

@Optional.Interface(
        iface = "blusunrize.immersiveengineering.api.energy.IRotationAcceptor",
        modid = "immersiveengineering"
)
public class TileEntitySturdyGearbox extends TileEntity implements ITickable, IRotationAcceptor {
    public EnumFacing facing;
    private IMechCapability mechCapability;
    private boolean broken = false;
    private boolean noInput = false;

    public TileEntitySturdyGearbox() {
        super();
        this.facing = EnumFacing.NORTH;
        this.mechCapability = new OutputMechCapability();
    }

    public TileEntitySturdyGearbox setFacing(EnumFacing facing) {
        this.facing = facing;
        return this;
    }

    @Override
    public boolean hasCapability(@NotNull Capability<?> capability, @Nullable EnumFacing facing) {
        if (capability == MECH_CAPABILITY && facing == this.facing.getOpposite()) {
            return true;
        }

        return super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(@NotNull Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == MECH_CAPABILITY && facing == this.facing.getOpposite()) {
            return (T) this.mechCapability;
        }

        return super.getCapability(capability, facing);
    }

    @Override
    public void update() {
        if(this.noInput) {
            mechCapability.setPower(0, null);
        }
        this.noInput = true;
    }

    @Override
    @Optional.Method(modid = "immersiveengineering")
    public void inputRotation(double v, @Nonnull EnumFacing from) {
        if(from.getOpposite() != this.facing || this.broken) {
            return;
        }

        this.noInput = false;
        this.mechCapability.setPower(
                Math.min(v * ModConfig.sturdyGearbox.conversionRate, ModConfig.sturdyGearbox.outputCap),
                null
        );
    }

    public void breakBlock() {
        this.mechCapability.setPower(0, null);
        this.broken = true;
    }

    class OutputMechCapability extends DefaultMechCapability {
        @Override
        public boolean isInput(EnumFacing from) {
            return false;
        }

        @Override
        public boolean isOutput(EnumFacing from) {
            return true;
        }

        @Override
        public void onPowerChange() {
            updateOutput();
        }
    }

    public void updateOutput() {
        TileEntity te = world.getTileEntity(pos.offset(facing.getOpposite()));

        if(te == null || !te.hasCapability(MECH_CAPABILITY, facing)) return;

        IMechCapability mech = te.getCapability(MECH_CAPABILITY, facing);

        if(!mech.isInput(facing)) return;

        mech.setPower(mechCapability.getPower(null), facing);
    }

    @NotNull
    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);
        mechCapability.writeToNBT(compound);
        compound.setString("facing", this.facing.getName2());
        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);
        mechCapability.readFromNBT(compound);
        this.facing = EnumFacing.byName(compound.getString("facing"));
        if(this.facing == null) this.facing = EnumFacing.NORTH;
    }
}
