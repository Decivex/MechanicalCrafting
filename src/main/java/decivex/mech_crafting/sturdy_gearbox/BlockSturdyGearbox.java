package decivex.mech_crafting.sturdy_gearbox;

import decivex.mech_crafting.MechCraftingMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;

public class BlockSturdyGearbox extends BlockHorizontal {
    public BlockSturdyGearbox() {
        super(Material.WOOD);
        this.setHardness(3.5f);
        this.setResistance(17.5f);
        this.setUnlocalizedName("sturdy_gearbox");
        this.setRegistryName(MechCraftingMod.MODID, "sturdy_gearbox");
        this.setCreativeTab(CreativeTabs.DECORATIONS);
    }

    @NotNull
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, FACING);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(FACING).getHorizontalIndex();
    }

    @NotNull
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState().withProperty(FACING, EnumFacing.getHorizontal(meta));
    }

    @NotNull
    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing face, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer){
        return this.getDefaultState().withProperty(FACING, placer.getHorizontalFacing().getOpposite());
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        return true;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(@NotNull World world, IBlockState state) {
        return new TileEntitySturdyGearbox().setFacing(state.getValue(FACING));
    }

    @Override
    public void neighborChanged(IBlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos) {
        super.neighborChanged(state, world, pos, blockIn, fromPos);

        TileEntitySturdyGearbox te = (TileEntitySturdyGearbox) world.getTileEntity(pos);
        te.updateOutput();
    }

    @Override
    public void breakBlock(World world, @NotNull BlockPos pos, @NotNull IBlockState state) {
        TileEntitySturdyGearbox te = (TileEntitySturdyGearbox) world.getTileEntity(pos);
        te.breakBlock();
        super.breakBlock(world, pos, state);
    }
}
