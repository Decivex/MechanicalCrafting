package decivex.mech_crafting;

import decivex.mech_crafting.mechanical_bellows.BlockMechanicalBellows;
import decivex.mech_crafting.mechanical_bellows.MechanicalBellowsTESR;
import decivex.mech_crafting.mechanical_bellows.TileEntityMechanicalBellows;
import decivex.mech_crafting.mechanical_workbench.BlockMechanicalWorkbench;
import decivex.mech_crafting.mechanical_workbench.TileEntityMechanicalWorkbench;
import decivex.mech_crafting.sturdy_gearbox.BlockSturdyGearbox;
import decivex.mech_crafting.sturdy_gearbox.TileEntitySturdyGearbox;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry.ObjectHolder;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;

@Mod.EventBusSubscriber
public class RegistryHandler {

    @ObjectHolder("mech_crafting:mechanical_crafter")
    public static Block MECHANICAL_CRAFTER = new BlockMechanicalWorkbench();

    @ObjectHolder("mech_crafting:sturdy_gearbox")
    public static Block STURDY_GEARBOX = new BlockSturdyGearbox();

    @ObjectHolder("mech_crafting:mechanical_bellows")
    public static Block MECHANICAL_BELLOWS = new BlockMechanicalBellows();

    @SubscribeEvent
    public void registerBlocks(RegistryEvent.Register<Block> event) {
        event.getRegistry().register(MECHANICAL_CRAFTER);
        event.getRegistry().register(STURDY_GEARBOX);
        event.getRegistry().register(MECHANICAL_BELLOWS);

        GameRegistry.registerTileEntity(
                TileEntityMechanicalWorkbench.class,
                MECHANICAL_CRAFTER.getRegistryName()
        );

        GameRegistry.registerTileEntity(
                TileEntitySturdyGearbox.class,
                STURDY_GEARBOX.getRegistryName()
        );

        GameRegistry.registerTileEntity(
                TileEntityMechanicalBellows.class,
                MECHANICAL_BELLOWS.getRegistryName()
        );
    }

    @SubscribeEvent
    public void registerItems(RegistryEvent.Register<Item> event) {
        event.getRegistry().register(new ItemBlock(MECHANICAL_CRAFTER).setRegistryName(MECHANICAL_CRAFTER.getRegistryName()));
        event.getRegistry().register(new ItemBlock(STURDY_GEARBOX).setRegistryName(STURDY_GEARBOX.getRegistryName()));
        event.getRegistry().register(new ItemBlock(MECHANICAL_BELLOWS).setRegistryName(MECHANICAL_BELLOWS.getRegistryName()));
    }

    @SideOnly(Side.CLIENT)
    @SubscribeEvent
    public void registerModels(ModelRegistryEvent event) {
        registerItemModel(Item.getItemFromBlock(MECHANICAL_CRAFTER), 0, "inventory");
        registerItemModel(Item.getItemFromBlock(STURDY_GEARBOX), 0, "inventory");
        registerItemModel(Item.getItemFromBlock(MECHANICAL_BELLOWS), 0, "inventory");

        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityMechanicalBellows.class, new MechanicalBellowsTESR());
    }

    @SideOnly(Side.CLIENT)
    public void registerItemModel(@Nonnull Item item, int meta, String variant) {
        ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(item.getRegistryName(), variant));
    }
}