package decivex.mech_crafting;

import decivex.mech_crafting.network.GuiProxy;
import decivex.mech_crafting.network.ModPacketHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = MechCraftingMod.MODID,
        name = MechCraftingMod.NAME,
        version = MechCraftingMod.VERSION,
        dependencies = "required-after:mysticalmechanics;"
)
@Mod.EventBusSubscriber
public class MechCraftingMod
{
    public static final String MODID = "mech_crafting";
    public static final String NAME = "Mechanical Crafting";
    public static final String VERSION = "1.2.1b";

    @Mod.Instance("mech_crafting")
    public static MechCraftingMod instance;
        
    @SidedProxy(serverSide="decivex.mech_crafting.ServerProxy", clientSide="decivex.mech_crafting.ClientProxy")
    public static ISidedProxy proxy;

    public Logger logger;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        MinecraftForge.EVENT_BUS.register(new RegistryHandler());
        MinecraftForge.EVENT_BUS.register(this);
        ModPacketHandler.registerMessages(MODID);
    }

    @EventHandler
    public void init(FMLInitializationEvent event)  {
        ConfigManager.sync(MODID, Config.Type.INSTANCE);
        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiProxy());
    }

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
        if(event.getModID().equals(MODID)) {
            ConfigManager.sync(MODID, Config.Type.INSTANCE);
        }
    }

}
