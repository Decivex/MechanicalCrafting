package decivex.mech_crafting;

import decivex.mech_crafting.network.PacketUpdateItemStack;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ServerProxy implements ISidedProxy {
    
    @Override
    public void handleUpdateItemStack(PacketUpdateItemStack message, MessageContext ctx) {
        MechCraftingMod.instance.logger.error("Got PacketUpdateItemStack on wrong side!");
    }
    
}